# Lsystog 

Lsystog is a python module to generate images.

Please have a look at the "sample_images" folder to see examples of generated images.

This image generation is inspired from [Lindenmayer Systems.](https://en.wikipedia.org/wiki/L-system)

## Setup Instructions

Install Required Python Modules

```bash
pip install -r requirements.txt
```

## Use cases

See or launch the files "samples*.py"
The generated images will be in the "sample_images" folder. 

You may slightly increase the number of iterations to improve the quality of the images.
You may decrease it to speed up the process.

## Notes

This project is an old personal project (the documentation and redesign are in progress).
The unit tests are not written yet and the type hinting is not used yet, unfortunately.

